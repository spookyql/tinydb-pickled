from tinydb import TinyDB, where
from tinydb_pickled import Storage, Table, IndexedDocument, HashableDocument
from tempfile import TemporaryDirectory
    
class DB(TinyDB):
    table_class = Table
    default_storage_class = Storage

from os import makedirs

def store():
    # with TemporaryDirectory() as root:
    root = "/tmp/tinydbpd"

    try:
        makedirs(root)
        initialize = True
    except FileExistsError:
        initialize = False
        
    print('root:', root)
    db = DB(path=root)

    artists = db.table('artists', Document=IndexedDocument.by('identifier'))
    groups = db.table('groups', Document=IndexedDocument.by('identifier'))
    memberships = db.table('memberships', Document=HashableDocument)

    if initialize:
        eschiele = artists.insert({'identifier': 'eschiele',
                                   'firstnames': ('Egon',),
                                   'familyname': 'Schiele',
                                   'email': 'egon@schiele.at'})
        ppicaso = artists.insert({'identifier': 'ppicaso',
                                  'firstnames': ('Pablo',),
                                  'familyname': 'Picaso',
                                  'email': 'pablo@picaso.es'})

        arodin = artists.insert({'identifier': 'arodin',
                                 'firstnames': ('Auguste',),
                                 'familyname': 'Rodin',
                                 'email': 'auguste@rodin.fr'})

        sculptors = groups.insert({'identifier':'sculptors', "description": "sculptors"})
        painters = groups.insert({'identifier':'painters', "description": "painters"})

        memberships.insert({'artist': eschiele, 'group': painters})
        memberships.insert({'artist': arodin, 'group': sculptors})

    '''delete from memberships
       where memberships.artist
             in (select artist.identifier
                 from artist
                 where artist.familyname = 'Picaso')
    '''
    memberships.remove(where('artist').one_of([artist.doc_id
                                             for artist
                                             in artists.search(where('familyname') == 'Picaso')]))

    '''select artists.identifier, groups.identifier
       from memberships
            inner join artists on (memberships.artist=artist.identifier)
            inner join groups on (memberships.group=groups.identifier)
    '''
    for membership in memberships.all():
        print(artists.get(doc_id=membership['artist'])['identifier'],
              'in',
              groups.get(doc_id=membership['group'])['identifier'])


    '''select artists.firstnames, artists.familyname, group_concat(groups.description)
       from artists, memberships, groups
       where memberships.artist=artist.identifier and memberships.group=groups.identifier
       group by artists.firstnames, artists.familyname
    '''
    '''
select artists.firstnames, artists.familyname
a
    '''
    for artist in artists.all():
        matching_groups = set()
        
        for membership in memberships.search(where('artist') == artist['identifier']):
            for group in groups.search(where('identifier') == membership['group']):
                matching_groups.add(group['description'])

        print(' '.join(artist['firstnames']), artist['familyname'], 'is a', matching_groups)
