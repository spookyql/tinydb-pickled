from collections.abc import Hashable
from os import listdir, makedirs, remove, rmdir
from os.path import join
from pickle import load, loads, dump, dumps
from hashlib import shake_128
from typing import Optional, Any, Callable, List, Dict, Iterable, Mapping, Union
from tinydb import Storage as _Storage
from tinydb.table import Table as _Table, Document as _Document
from tinydb.queries import QueryLike

class Storage(_Storage):
    def __init__(self, path):
        super().__init__()
        self.path = path
        makedirs(path, exist_ok=True)

    def read(self) -> Optional[Dict[str, Dict[Hashable, Any]]]:
        data = {}
        
        for name in listdir(self.path):
            table = {}

            for key in listdir(join(self.path, name)):
                with open(join(self.path, name, key), 'rb') as file:
                    table[loads(bytes.fromhex(key))] = load(file)
            
            data[name] = table

        return data

    def write(self, data: Dict[str, Dict[str, Any]]):
        for name in listdir(self.path):
            if name not in data.keys():
                for filename in listdir(join(self.path, name)):
                    remove(join(self.path, name, filename))
                rmdir(name)

        for name in data.keys():
            if name not in listdir(self.path):
                makedirs(join(self.path, name))
        
        
        for name, table in data.items():
            for key in listdir(join(self.path, name)):
                if loads(bytes.fromhex(key)) not in table.keys():
                    remove(join(self.path, name, key))
                
            for key, entry in table.items():
                with open(join(self.path, name, dumps(key).hex()), 'wb') as file:
                    dump(entry, file)

# class Identifier:
#     def __init__(self, table, document):
#         raise NotImplemented()

#     @classmethod
#     def from_storage(cls, table, Hashable):
#         raise NotImplemented()

#     def to_storage(self) -> Hashable:
#         raise NotImplemented()

# class ShakeIdentifier(Identifier):
#     def __init__(self, table, document)
#         if self.key is None:
#             keys = list(value.keys())
#             keys.sort()
#             hsh = tuple((key, value[key])
#                         for key in keys)
#         else:
#             hsh = value[self.key]

#         return shake_128(dumps(hsh)).digest(self._doc_id_length)

#     @staticmethod
#     def 

class Document(_Document):
    def __init__(self, value):
        super().__init__(value, None)
        self.doc_id = self._compute_doc_id()

class HashableDocument(Document):
    _doc_id_length = 8
    
    def update(self, value):
        keys = set(value.keys())
        
        if (keys.intersection(set(self.keys())) != keys
            or any(value[key] != self[key] for key in keys)):
            self.doc_id = self._compute_doc_id()
        
        super().update(value)
    
    def __setitem__(self, key, value):
        previous_value = self.get(key, None)
        
        super().__setitem__(key, value)
        
        if value != previous_value:
            self.doc_id = self._compute_doc_id()

    def _compute_doc_id(self) -> Hashable:
        return shake_128(dumps(frozenset(self.items()))).digest(self._doc_id_length)

class IndexedDocument(Document):
    key:None|str = None

    @staticmethod
    def _inflate(key, document):
        return IndexedDocument.by(key)(document)
    
    def __reduce__(self):
        return (self._inflate, (self.key, dict(self)))

    @classmethod
    def by(cls, key: str):
        return type(cls.__name__ + '_By_' + key,
                    (cls,),
                    {'key': key})
    
    def update(self, value):
        if (self.key in values.keys()
            and value[self.key] != self[self.key]):
            self.doc_id = value[self.key]

        super().update(value)

    def __setitem__(self, key, value):
        if key == self.key and value != self[key]:
            self.doc_id = value
        
        super().__setitem__(key, value)
    
    def _compute_doc_id(self) -> Hashable:
        return self[self.key]


class Table(_Table):
    def __init__(self, *args, Document:type[Document]=Document, **kargs):
        super().__init__(*args, **kargs)
        self.document_class = Document
        self.document_id_class = lambda x:x
    
    def insert(self, document:Mapping) -> Any:
        if not isinstance(document, Document):
            document = self.document_class(document)

        return super().insert(document)

    def insert_multiple(self, documents: Iterable[Mapping]) -> List[Any]:
        return super().insert_multiple((document
                                        if isinstance(document, self.document_class)
                                        else self.document_class(document))
                                       for document
                                       in documents)

    def update(
        self,
        fields: Union[Mapping, Callable[[Mapping], None]],
        cond: Optional[QueryLike] = None,
        doc_ids: Optional[Iterable[int]] = None,
    ) -> List[int]:
        def _fields(document):
            doc_id = document.doc_id
            
            if callable(fields):
                fields(document)
            else:
                document.update(fields)
                
            if document.doc_id != doc_id:
                del self[doc_id]
                self[document.doc_id] = document
        
        return super().update(_fields, cond, doc_ids)


    def search(self, cond: QueryLike) -> List[Document]:
        cached_results = self._query_cache.get(cond)
        if cached_results is not None:
            return cached_results[:]

        # Perform the search by applying the query to all documents.
        # Then, only if the document matches the query, convert it
        # to the document class and document ID class.
        docs = [
            doc
            for doc in self._read_table().values()
            if cond(doc)
        ]

        is_cacheable: Callable[[], bool] = getattr(cond, 'is_cacheable',
                                                   lambda: True)
        if is_cacheable():
            # Update the query cache
            self._query_cache[cond] = list(docs)

        return docs
    
    def get(
        self,
        cond: Optional[QueryLike] = None,
        doc_id: Optional[int] = None,
        doc_ids: Optional[List] = None
    ) -> Optional[Union[Document, List[Document]]]:
        """
        Get exactly one document specified by a query or a document ID.
        However, if multiple document IDs are given then returns all
        documents in a list.
        
        Returns ``None`` if the document doesn't exist.

        :param cond: the condition to check against
        :param doc_id: the document's ID
        :param doc_ids: the document's IDs(multiple)

        :returns: the document(s) or ``None``
        """
        table = self._read_table()

        if doc_id is not None:
            # Retrieve a document specified by its ID
            raw_doc = table.get(doc_id, None)

            if raw_doc is None:
                return None

            # Convert the raw data to the document class
            return raw_doc

        elif doc_ids is not None:
            # Filter the table by extracting out all those documents which
            # have doc id specified in the doc_id list.

            # Since document IDs will be unique, we make it a set to ensure
            # constant time lookup
            doc_ids_set = set(doc_id for doc_id in doc_ids)

            # Now return the filtered documents in form of list
            return [
                doc
                for doc_id, doc in table.items()
                if doc_id in doc_ids_set
            ]

        elif cond is not None:
            # Find a document specified by a query
            # The trailing underscore in doc_id_ is needed so MyPy
            # doesn't think that `doc_id_` (which is a string) needs
            # to have the same type as `doc_id` which is this function's
            # parameter and is an optional `int`.
            for doc_id_, doc in self._read_table().items():
                if cond(doc):
                    return self.document_class(doc)

            return None

        raise RuntimeError('You have to pass either cond or doc_id or doc_ids')

    def _read_table(self) -> Dict[str, Mapping]:
        """
        Read the table data from the underlying storage.

        Documents and doc_ids are NOT yet transformed, as 
        we may not want to convert *all* documents when returning
        only one document for example.
        """

        # Retrieve the tables from the storage
        tables = self._storage.read()

        if tables is None:
            # The database is empty
            return {}

        if not self.name in tables.keys():
            # no such table in database
            return {}

        table = tables[self.name]
        
        for key, document in table.items():
            if not isinstance(document, self.document_class):
                document = self.document_class(document)

            if not document.doc_id == key:
                del table[key]
                table[document.doc_id] = document
            else:
                table[key] = document
        
        return table


    def __iter__(self):
        return iter(self._read_table().values())
    
    def _update_table(self, updater: Callable[[Dict[int, Mapping]], None]):
        """
        Perform a table update operation.

        The storage interface used by TinyDB only allows to read/write the
        complete database data, but not modifying only portions of it. Thus,
        to only update portions of the table data, we first perform a read
        operation, perform the update on the table data and then write
        the updated data back to the storage.

        As a further optimization, we don't convert the documents into the
        document class, as the table data will *not* be returned to the user.
        """

        tables = self._storage.read()
        table = self._read_table()
        
        # Perform the table update operation
        updater(table)

        tables[self.name] = table

        # Write the newly updated data back to the storage
        self._storage.write(tables)

        # Clear the query cache, as the table contents have changed
        self.clear_cache()

